// [SECTION] Comparison Query Operators

// $gt/$gte Operator
/*
  Allows us to find documents that have field number values greater than or equal to a specified value

  Syntax:
    db.collectionName.find({field: { $gt : value }})
    db.collectionName.find({field: { $gte : value }})
*/
db.users.find({ age: { $gt: 50 } })
db.users.find({ age: { $gte: 50 } })

// $lt/$lte Operator
/*
  Allows us to find documents that have field number values less than or equal to a specified value

  Syntax:
    db.collectionName.find({field: { $lt : value }})
    db.collectionName.find({field: { $lte : value }})
*/
db.users.find({ age: { $lt: 50 } })
db.users.find({ age: { $lte: 50 } })

// $ne Operator
/*
  Allows us to find documents that have field number values not equal to a specified value

  Syntax:
    db.collectionName.find({ field : { $ne : value} })
*/
db.users.find({ age: { $ne: 82 } })

// $in Operator
/*
  Allows us to find documents with specific match criteria using different values

  Syntax:
    db.collectionName.find({ field: { $in: value }})
*/
db.users.find({ lastName: { $in: ['Hawking', 'Doe'] } })
db.users.find({ courses: { $in: ['HTML', 'React'] } })

// [SECTION] Logical Query Operators
// $or Operator
/*
  Allows us to find documents that match a single criteria from a multiple provided search criteria

  Syntax:
    db.collectionName.find({ $or: [ { field: value }, { field2: value2 }] })
*/
db.users.find({ $or: [{ firstName: 'Neil' }, { age: 25 }] })
db.users.find({ $or: [{ firstName: 'Neil' }, { age: { $gt: 25 } }] })

// $and Operator
/*
  Allows us to find documents matching multiple criteria in a single field

  Syntax:
    db.collectionName.find({ $and: [filed1 : value1}]})
*/
db.users.find({ $and: [{ age: { $ne: 82 } }, { age: { $ne: 76 } }] })

// [SECTION] Field Projection:
// 1) Inclusion
/*
  - Allows us to include specific field/s when retrieving documents
  - The value provided is 1 to denote that the field is being included

  Syntax:
    db.collectionName.find({ criteria }, { field: 1 })
*/
db.users.find(
  {
    firstName: 'Jane',
  },
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
  }
)
// 2) Exclusion
/*
  - Allows us to exclude specific field/s when retrieving documents
  - The value provided is 0 to denote that the field is being excluded

  Syntax:
    db.collectionName.find({ criteria }, { field: 0 })
*/
db.users.find(
  {
    firstName: 'Jane',
  },
  {
    contact: 0,
    department: 0,
  }
)
// Supressing the ID field
/*
- Allows us to exclude the "_id" when retrieving documents
- When using the field projection, field inclusion and exclusion cannot be used at the same time
  - the only exception is excluding the "_id"
*/
db.users.find(
  {
    firstName: 'Jane',
  },
  {
    _id: 0,
    contact: 0,
    department: 0,
  }
)

// Returning specific fields in Embedded documents
db.users.find(
  {
    firstName: 'Jane',
  },
  {
    firstName: 1,
    lastName: 1,
    'contact.phone': 1,
  }
)
/*
  Mini Activity #1:
    Return all fields except the phone field embedded in contact field
*/
db.users.find(
  {
    firstName: 'Jane',
  },
  {
    'contact.phone': 0,
  }
)
// Evaluation Query Operators
/*
  $regex Operator
    - Allows us to find documents that allows to find documents that match a specific pattern using regular expression

    Syntax:
      db.collectionName.find({ field: { $regex: 'pattern', $options: '$optionValue' } })
*/

// Case sensitive query
db.users.find({ firstName: { $regex: 'N' } })

// Case insensitive query
db.users.find({ firstName: { $regex: 'j', $options: '$i' } })
